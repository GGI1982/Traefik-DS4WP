# Traefik-DS4WP

[![Build Status](https://travis-ci.com/GGI1982/Traefik-DS4WP.svg?branch=master)](https://travis-ci.com/GGI1982/Traefik-DS4WP)

Traefik stack to be used with [DS4WP](https://gitlab.com/GGI1982/DS4WP)

If you are willing to run multiple websites (and so multiple stacks) on a single host you will need to comment the Traefik service from the docker-compose file of each of your [DS4WP project](https://gitlab.com/GGI1982/DS4WP) and implement a Traefik out-of-the-box in order to prevent reserved ports. 

Once you have disabled the Traefik service in your [DS4WP project](https://gitlab.com/GGI1982/DS4WP) the only thing you have to do is to customize the [docker-compose.yml](https://gitlab.com/GGI1982/Traefik-DS4WP/blob/master/docker-compose.yml "docker-compose.yml") file to add needed networks for each project.

   
 ## How to use
First of all you have to set your contact email address for the request of HTTPS certificat by changing line 15 of [docker-compose.yml](https://gitlab.com/GGI1982/Traefik-DS4WP/blob/master/docker-compose.yml "docker-compose.yml") file.

    - '--certificatesresolvers.myhttpchallenge.acme.email=youremail@gmail.com'


If the value of the **PROJECT_NAME** environment variable in the [.env file](https://gitlab.com/GGI1982/DS4WP/blob/master/.env) is **myproject** then you should modify  [docker-compose.yml](https://gitlab.com/GGI1982/Traefik-DS4WP/blob/master/docker-compose.yml "docker-compose.yml") of this repository.

    networks:
    network1:
        external:
            name: projectname1_default
Would become :

    networks:
    network1:
        external:
            name: myproject_default
You can add as many networks you need for as many project [DS4WP project](https://gitlab.com/GGI1982/DS4WP) you are willing to run on the same host.

    networks:
        - network1
        - network2
        - network3
        - network4
        - network..
        - ...
        - 
     
     networks:
     network1:
        external:
            name: projectname1_default
     network2:
        external:
            name: projectname2_default
     network3:
        external:
            name: projectname3_default
     network4:
        external:
            name: projectname4_default                     
   
